#!/usr/bin/env python3

import os
import datetime
import reports


attachment = "/tmp/processed.pdf"
path = "supplier-data/descriptions"


def extract_from_txt(path):
    for (root, dirs, files) in os.walk(path):
        for file in files:
            if file.endswith(".txt"):
                with open(file, 'rb') as f:
                    report_string = ""
                    name = f.readline().strip()
                    weight = f.readline().strip()
                    report_string += "name: " + name + "<br />" + "weight: " + weight + "<br />" + "<br />"
    return report_string


if __name__ == "__main__":
    title = "Processed Update on {}".format(datetime.date.today())
    paragraph = extract_from_txt(path)
    reports.generate_report(paragraph, title, attachment)
