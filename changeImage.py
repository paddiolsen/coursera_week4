# !/usr/bin/env python3

import os
import re
from PIL import Image


def pil_convert_to_jpg(path):
    """ Opens up a tiff file and converts it in to a jpg with size 3000x2000
    the function, return file path for each jpeg file
    """

    for (root, dirs, files) in os.walk(path):
        for file in files:
            if 'tiff' in file:
                # Open file
                im = Image.open("{}{}".format(path, file))

                # resize and change from RGBA to RGB
                im = im.convert("RGB")
                im = im.resize((600, 400))

                # create new path for file
                name_of_file = re.sub(r'.tiff', "", file)
                # FILE_NAME.append(path + "" + name_of_file + ".jpeg")
                jpg_name = im.save("{}{}.jpeg".format(path, name_of_file))


if __name__ == "__main__":
    pil_convert_to_jpg("supplier-data/images/")

