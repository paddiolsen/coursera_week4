#!/usr/bin/env python3
import os
import shutil
import socket
import psutil
import emails


def check_cpu_usage():
    '''Report an error if CPU usage is over 80%'''
    usage = psutil.cpu_percent(1)
    print(usage)
    return usage < 80


def low_disk_space():
    '''Report an error if available disk space is lower than 20%'''
    disk_space = shutil.disk_usage("/")
    free_space_procent = disk_space.free * 100 / disk_space.total
    return free_space_procent > 20


def low_memory():
    '''Report an error if available memory is less than 500MB'''
    total_memory = psutil.virtual_memory().available
    total_memory_mb = total_memory / 1024 ** 2
    return total_memory_mb > 500


def localhost_error():
    '''Report an error if the hostname "localhost" cannot be resolved to "127.0.0.1"'''
    localhost_ip = socket.gethostbyname('localhost')
    return localhost_ip == '127.0.0.1'

def email_warning(subject_line):
    sender = "automation@example.com"
    recipient = "{}@example.com".format(os.environ['USER'])
    subject = subject_line
    body = "Please check your system and resolve the issue as soon as possible."
    email = emails.generate_email(sender, recipient, subject, body)
    emails.send(email)


if __name__ == "__main__":
    if not check_cpu_usage():
        subject_line = "Error - CPU usage is over 80%"
        email_warning(subject_line)

    if not low_disk_space():
        subject_line = "Error - Available disk space is less than 20%"
        email_warning(subject_line)

    if not low_memory():
        subject_line = "Error - Available memory is less than 500MB"
        email_warning(subject_line)

    if not localhost_error():
        subject_line = "Error - localhost cannot be resolved to 127.0.0.1"
        email_warning(subject_line)