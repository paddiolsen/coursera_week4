import unittest
from PIL import Image

from changeImage import pil_convert_to_jpg


class MyTestCase(unittest.TestCase):
    def test_jpeg_extension(self):
        path = "/home/paddi/Desktop/scripts/coursera_week4/pic/"
        expected = ["/home/paddi/Desktop/scripts/coursera_week4/pic/5.jpg"]
        self.assertEqual(pil_convert_to_jpg(path), expected)


if __name__ == '__main__':
    unittest.main()
