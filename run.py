#! /usr/bin/env python3
import json
import os
import requests

path = "supplier-data/descriptions/"
url = "http://localhost/fruits/"
data_dict = {
    "name": "",
    "weight": "",
    "description": "",
    "image_name": ""
}



def data_uploader():
    ''' For each txt file in folder, read the lines and add data to
    to the dict, then convert the dict to json and sendt the json data with post
    request
    '''
    for (root, dirs, files) in os.walk(path):
        for file in files:
            with open(file, 'rb') as file:
                # add the name to dict
                name = file.readline()
                data_dict["name"] = name
                # remove lbs in weight and add weight to dict
                weight = file.readline()
                weight = weight.strip(".lbs")
                data_dict["weight"] = weight
                #  add description
                description = file.readline()
                data_dict["description"] = description
                #  add image file
                image_file = file.strip(".txt")
                data_dict["image_name"] = "{}.jpeg".format(image_file)

    #json_obj = json.dumps(data_dict)
    r = requests.post(url, data_dict)
    r.raise_for_status()
