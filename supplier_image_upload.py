#!/usr/bin/env python3

import os
import requests


url = ""


def upload_images(path):
    for (root, dirs, files) in os.walk(path):
        for file in files:
            with open(file, 'rb') as opened:
                r = requests.post(url, files={'file': opened})


if __name__ == "__main__":
    upload_images("supplier-data/images")
